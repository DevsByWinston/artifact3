// Fill out your copyright notice in the Description page of Project Settings.

#include "Target.h"
#include "Engine.h" //link to header for constructorhelpers


// Sets default values
ATarget::ATarget()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bTargetHit = false; //target isnt hit yet
	//load mesh from engine cyl mesh & assign to component
	m_targetMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cyl Mesh Target"));
	RootComponent = m_targetMesh;
	{ //pull static mesh and assign to component
		static ConstructorHelpers::FObjectFinder <UStaticMesh> asset(TEXT("/Engine/BasicShapes/Cylinder.Cylinder")); //findobject
		m_targetMesh->SetStaticMesh(asset.Object); //assign to mesh component
	}
	m_targetMesh->SetRelativeScale3D(FVector(.5, .5, 0.05)); //set size of static mesh

		//set collisionprofile & OnhitComp
	m_targetMesh->SetCollisionProfileName("BlockAllDynamic");
	m_targetMesh->OnComponentHit.AddDynamic(this, &ATarget::OnCompHit);

	//vectors variables for rotation
	FRotator Rotation;
	Rotation.Yaw = 0.0f;
	Rotation.Pitch = 90.0f;
	Rotation.Roll = 0.0f;

	m_targetMesh->SetRelativeRotation(Rotation); //assign property to component

	//load material
	{
		static ConstructorHelpers::FObjectFinder <UMaterial> asset(TEXT("Material'/Game/Shaders/TargetShader.TargetShader'"));
		m_dynamicMaterial = asset.Object;
	}
	m_targetMesh->SetMaterial(0, m_dynamicMaterial); //assign material to mesh component
}

void ATarget::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void ATarget::OnCompHit(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	bTargetHit = true;
}

// Called when the game starts or when spawned
void ATarget::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATarget::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bTargetHit)
	{
		if (AAmount > -5.0f)
		{
			AAmount = -5.0f;
		}
		bTargetHit = false;
	}

	if (!bTargetHit)
	{
		if (AAmount < 1.0f)
		{
			AAmount += DeltaTime;
		}
	}
	//reference to material to create instance.
	UMaterialInstanceDynamic* HitMaterialInstance = m_targetMesh->CreateDynamicMaterialInstance(0);

	//check if initializatiion suceeded
	if (HitMaterialInstance != nullptr)
	{
		HitMaterialInstance->SetScalarParameterValue(FName("AAmount"), AAmount); //update alpha value.
	}
}

