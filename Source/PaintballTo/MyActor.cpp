// Fill out your copyright notice in the Description page of Project Settings.

#include "MyActor.h"


// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false; //disable tick every frame, not necessary

	//assign variabletype proceduralmeshomponent named GeneratedMesh
	mesh = CreateDefaultSubobject <UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	//make it the root componenent
	RootComponent = mesh;
}

//called when actor is already in level and map is opened
void AMyActor::PostActorCreated()
{
	Super::PostActorCreated();
	CreateSquare(); //call createsquare function
}

void AMyActor::PostLoad()
{
	Super::PostLoad();
	CreateSquare();
}

//generate a 3d object, square and load it into the level
void AMyActor::CreateSquare()
{
	TArray <FVector> Vertices; //variable for vertex buffer
	TArray <int32> Triangles; //variable for index/triangle buffeer
	TArray <FVector> Normals;
	TArray <FLinearColor> Colors;
	Vertices.Add(FVector(0.f, 0.f, 0.f));
	Vertices.Add(FVector(0.f, 100.f, 0.f));
	Vertices.Add(FVector(0.f, 0.f, 100.f));
	Vertices.Add(FVector(0.f, 100.f, 100.f));
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
	Triangles.Add(3);
	Triangles.Add(2);
	Triangles.Add(1);
	//loop through vertices and add vector to nnormal and color
	for (int32 i = 0; i < Vertices.Num(); i++) {
		Normals.Add(FVector(0.f, 0.f, 1.f));
		Colors.Add(FLinearColor::Red);
	}//optional array    
	TArray <FVector2D> UV0;
	TArray <FProcMeshTangent> Tangents;
	//add 2d object to screen with proceduralmesh generation.
	mesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UV0, Colors, Tangents, true);

}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

