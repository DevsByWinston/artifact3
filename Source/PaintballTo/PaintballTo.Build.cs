// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PaintballTo : ModuleRules
{
	public PaintballTo(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        //need to add procedural mesh component into public dependencies in order to refrence the plugin
        //add runtimemeshcomponent dependcie
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "ProceduralMeshComponent", "RuntimeMeshComponent", "RHI", "PixelShader", "ComputeShader" }); 
	}
}
