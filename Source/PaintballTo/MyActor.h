// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//include header files for ProceduralMeshComponent
#include "ProceduralMeshComponent.h"
#include "MyActor.generated.h"

UCLASS()
class PAINTBALLTO_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

	// declare functions to generate a procedural mesh component
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void CreateSquare();

private:
	//make UPROPERTY ProcedualMeshComponent visible in editor
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent * mesh; //declare variable type proceduralmeshcomponent named mesh

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
