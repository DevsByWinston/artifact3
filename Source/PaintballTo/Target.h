// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Target.generated.h"

UCLASS()
class PAINTBALLTO_API ATarget : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATarget();

	//check if comp have been initialized
	virtual void PostInitializeComponents() override;

	//declaring components
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent * m_targetMesh;
	UPROPERTY()
		UTexture2D * m_dynamicTexture;
	UPROPERTY()
		UMaterialInterface * m_dynamicMaterial;
	UPROPERTY()
		UMaterialInstanceDynamic * m_dynamicMaterialInstannce;

	//trigger function to dynamically change shader
	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

private:
	
	//variables
	bool bTargetHit; //on/off bool
	float AAmount; //alpha shader parameter

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
