// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "PaintballToProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h" //use to grab our pieces the actual content broowser
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h" // use to get random int in range
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Engine/DecalActor.h" // driver
#include "Components/DecalComponent.h" //Decal functions
#include "Components/SphereComponent.h"

APaintballToProjectile::APaintballToProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &APaintballToProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	//When you set up color for projectile NOTE: instantiate static mesh to have access to variable in visual editor. static mesh must be connected to root comp.
	//FObjectFinder Pulls Asset.
	static ConstructorHelpers::FObjectFinder<UMaterial>Material(TEXT("Material'/Game/Splat/PaintSplat_MAT.PaintSplat_MAT'"));
	
	if (Material.Object != NULL) //check if loaded properly
	{
		DecalMat = (UMaterial*)Material.Object; //set DecalMat to PaintSplat_MAT
	}
	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

void APaintballToProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{

	//PAINT SPLATTER CODE.
	if (OtherActor != NULL)
	{
		if (DecalMat != nullptr) //if DecalMat is not null
		{
			//SpawnDecalAtLocation() at where the projectile hits. Takes in Location, Material. RandomFloatInRange sets the decal size 20.f, 40.f. Pass in hit Location and Normal rotation for surface normal, last parameter is lifetime. 
			//setting 1min paint time. 60.0f
			auto Decal = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), DecalMat, FVector(UKismetMathLibrary::RandomFloatInRange(20.f, 40.f)), Hit.Location, Hit.Normal.Rotation(), 60.0f);

			//create MatInstance
			auto MatInstance = Decal->CreateDynamicMaterialInstance();

			//set up FRAME scalarparameter() & vector COLOR parameter so it randomizes Splat decal
			MatInstance->SetScalarParameterValue("Frame", UKismetMathLibrary::RandomIntegerInRange(0, 3)); //randomize SplatTexture
			//FLinearColor(R,G,B) RGB are float values. Using Larger range 0, 10 to get glowing colors because "Color" is plugged into Emissive in the Material
			MatInstance->SetVectorParameterValue("Color", FLinearColor(UKismetMathLibrary::RandomFloatInRange(0.0f, 10.0f), UKismetMathLibrary::RandomFloatInRange(0.0f, 10.0f), UKismetMathLibrary::RandomFloatInRange(0.0f, 10.0f))); //flinear color has 3 inputs. use RandomFloatInRange to generate random float value. color .3 red .5 greeen .4 blue

			//destroy projectile
			//Destroy();

		}
	}
	

	// Only add impulse and destroy projectile if we hit a physics
	//HITS OBJECT, OTHER OBJECT IS NOT A PROJECTILE AND OTHER OBJECT IS NOT SIMULATING PHYSICS.
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

		//Destroy();
	}
}