// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "PaintballToGameMode.h"
#include "PaintballToHUD.h"
#include "PaintballToCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintballToGameMode::APaintballToGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintballToHUD::StaticClass();
}
